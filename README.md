# hora-deployment

rahasak blockchain based hora platfrom deployments

### services

following are the servies

```
1. zookeeper
2. kafka
3. redis
4. elassandra
5. kibana
6. aplos
7. groop
```

### deploy with single kafka broker

start services in following order

```
docker-compose up -d zookeeper
docker-compose up -d kafka
docker-compose up -d redis
docker-compose up -d elassandra
docker-compose up -d kibana
docker-compose up -d aplos
docker-compose up -d groop
```

### deploy with multiple kafka brokers

there is a seperate docker compose if you want to setup kafka multiple brokers

```
# first deploy kafka cluster
docker-compose -f kafka-compose.yml up -d

# deploy the rahasak services in following order
docker-compose up -d redis
docker-compose up -d elassandra
docker-compose up -d kibana
docker-compose up -d aplos
docker-compose up -d groop
```

### test services kafkacat

Aplos service consume messages from `aplos` kafka topic. We can publish messages 
to `aplos` topic to test the service. Need to install kafkacat command line tool. 

```
kafkacat -P -b localhost:9092 -t aplos
{"messageType":"create", "execer":"eranga", "id":"1111", "watchId":"1188", "watchModel":"UL120", "watchColor":"silver", "watchOwner":"eranga"}
```

### test services gateway

there is gateway service which exposes HTTP endpoints to POST messages to aplos
service.

```
# create
curl -XPOST localhost:8761/api/watches -d '
{"messageType":"create", "execer":"eranga", "id":"511115", "watchId":"21188", "watchModel":"tis20", "watchColor":"white", "watchOwner":"issa"}'

# transfer
curl -XPOST localhost:8761/api/watches -d '
{"messageType":"transfer", "execer":"eranga", "id":"511115", "watchId":"21188", "watchModel":"tis20", "watchColor":"white", "watchOwner":"asitha"}'
```
